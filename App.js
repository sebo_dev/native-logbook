import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { Provider as PaperProvider, Text, Divider, Button, FAB, TextInput, Card, Avatar, Paragraph, Title, Appbar, Menu} from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

export default function App() {
  return (
    <PaperProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home" screenOptions={{ header: (props) => <CustomNavigationBar {...props} />,}}>
          <Stack.Screen name="Home" component={HomeScreen} options={{ title: 'Welcome' }} />
          <Stack.Screen name="NewTrip" component={NewTrip} />
          <Stack.Screen name="EndTrip" component={EndTrip} />
          <Stack.Screen name="TripSummary" component={TripSummary} />
          <Stack.Screen name="AllTrips" component={AllTrips} />
          <Stack.Screen name="Contact" component={Contact} />
        </Stack.Navigator>
        
      </NavigationContainer>
    </PaperProvider>
  );
}

function CustomNavigationBar({ navigation, previous }) {
  const [visible, setVisible] = React.useState(false);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);

  return (
    <Appbar.Header>
      {previous ? <Appbar.BackAction onPress={navigation.goBack} /> : null}
      <Appbar.Content title="Logbook" />
      {!previous ? (
        <Menu
          visible={visible}
          onDismiss={closeMenu}
          anchor={
            <Appbar.Action icon="menu" color="white" onPress={openMenu} />
          }>
          <Menu.Item onPress={() => navigation.navigate('NewTrip')} title="New Trip" />
          <Menu.Item onPress={() => navigation.navigate('AllTrips')} title="History" />
          <Menu.Item onPress={() => navigation.navigate('Contact')} title="Contact" />
        </Menu>
      ) : null}
    </Appbar.Header>
  );
}

const HomeScreen = ({ navigation }) => {

  return (
    <View style={styles.container}>
      <Title>Welcome in logbook</Title>
      <Divider />
      <StatusBar style="auto" />
      <Button icon="history" onPress={() => navigation.navigate('AllTrips')}>History</Button>
      <Button icon="chat" onPress={() => navigation.navigate('Contact')}>Contact</Button>
      <FAB style={styles.fab} label="New Trip" small icon="plus" onPress={() => navigation.navigate('NewTrip')}/>
    </View>
    
  );
};

const NewTrip = ({ navigation, route }) => {

  const [text, setText] = React.useState('');

  return(
    <View>
      <Title>Start a new Trip</Title>
      <TextInput label="Milage" value={text} mode="outlined" onChangeText={text => setText(text)} />
      <Button icon="arrow-right" onPress={() => navigation.navigate('EndTrip', {StartMilage: text} )}>Start trip</Button>
      <Text>{text}</Text>
    </View>
  )
};

const EndTrip = ({ navigation, route }) => {

  const {StartMilage} = route.params;
  const [text, setText] = React.useState('');
  
  return(
    <View>
      <Title>End Trip</Title>
      <TextInput label="Milage" value={text} mode="outlined" onChangeText={text => setText(text)} />
      <Button icon="arrow-right" onPress={() => navigation.navigate('TripSummary', {StartMilage: StartMilage, EndMilage: text})}>End trip</Button>
      <Text>{StartMilage},{text}</Text>
    </View>
  )
};

const TripSummary = ({ navigation, route }) => {

  const {StartMilage} = route.params;
  const {EndMilage} = route.params;
  var Distance = EndMilage - StartMilage;
  
  return(
    <View style={styles.container}>
      <Title>Trip Summary</Title>
      <Paragraph>StartMilage: {StartMilage}</Paragraph>
      <Paragraph>EndMilage: {EndMilage}</Paragraph>
      <Paragraph>Distance: {Distance}</Paragraph>
      <Button icon="home" mode="contained" onPress={() => navigation.navigate('Home')}>Back to Home</Button>
    </View>
  )
};

const LeftContent = props => <Avatar.Icon {...props} icon="road-variant" />

const AllTrips = ({ navigation, route }) => {
  
  return(
    <ScrollView style={styles.scrollView}>
      <Card>
        <Card.Title title="Trip 20.01.2021" subtitle="5km" left={LeftContent} />
        <Card.Content>
          <Paragraph>
            2313124km - 1232131km
          </Paragraph>
        </Card.Content>
      </Card>
      <Card>
        <Card.Title title="Trip 12.02.2021" subtitle="5km" left={LeftContent} />
        <Card.Content>
          <Paragraph>
            2313124km - 1232131km
          </Paragraph>
        </Card.Content>
      </Card>
      <Card>
        <Card.Title title="Trip 04.03.2021" subtitle="5km" left={LeftContent} />
        <Card.Content>
          <Paragraph>
            2313124km - 1232131km
          </Paragraph>
        </Card.Content>
      </Card>
      <Card>
        <Card.Title title="Trip 07.03.2021" subtitle="5km" left={LeftContent} />
        <Card.Content>
          <Paragraph>
            2313124km - 1232131km
          </Paragraph>
        </Card.Content>
      </Card>

    </ScrollView>
  )
};

const Contact = ({ navigation, route }) => {
  return <Text>##CONTACT##</Text>;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
});
